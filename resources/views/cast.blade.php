@extends('layouts.master')
@section('title', 'Daftar Nama Cast')
@section('h1', 'Nama Cast Film')
@section('content')
<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Biography</th>
        <th>Edit Cast</th>
      </tr>
    </thead>
    @foreach ($casts as $key => $cast)
    <tbody>
      <tr>
          <td>{{ $key + 1 }}</td>
        <td>{{ $cast->nama }}</td>
        <td>
            {{ $cast->umur }} Tahun
        </td>
        <td>{{ $cast->bio }}</td>
        <td><a href="/cast/{{ $cast->id }}/edit">Edit</a></td>
        <td><form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Delete">
        </form></td>
      </tr>
    </tbody>
@endforeach
</table>
@endsection