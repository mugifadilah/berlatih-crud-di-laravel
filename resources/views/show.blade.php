@extends('layouts.master')
@section('title', 'Daftar Nama Cast')
@section('h1', 'Nama Cast Film')
@section('content')
<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Biography</th>
        <th>Edit Cast</th>
      </tr>
    </thead>
    @forelse ($shows as $key => $show)
    <tbody>
      <tr>
        <td>{{ $show->nama }}</td>
        <td>
            {{ $show->umur }} Tahun
        </td>
        <td>{{ $show->bio }}</td>
        <td><a href="/cast/{{ $show->id }}/edit">Edit</a></td>
        <td><form action="/cast/{{ $show->id }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Delete">
        </form></td>
      </tr>
      @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
    </tbody>
    @endforelse
</table>
@endsection