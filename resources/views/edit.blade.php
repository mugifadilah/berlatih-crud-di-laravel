@extends('layouts.master')
@section('title', 'Daftar Nama Cast')
@section('h1', 'Nama Cast Film')
@section('content')
<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Biography</th>
        <th>Edit Cast</th>
      </tr>
    </thead>
    @foreach ($casts as $key => $cast)
    <tbody>
      <tr>
          <td>{{ $key + 1 }}</td>
        <td>{{ $cast->nama }}</td>
        <td>
            {{ $cast->umur }} Tahun
        </td>
        <td>{{ $cast->bio }}</td>
        <td><a href="/cast/{{ $cast->id }}/edit">Edit</a></td>
        <td><form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Delete">
        </form></td>
      </tr>
    </tbody>
@endforeach
</table>
@endsection
@extends('layouts.master')
@section('title', 'Input Cast Baru')
@section('h1', 'Input Cast Baru')
@section('content')
    <div class="container mb-5">
        <div class="row" style="justify-content: center">
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group mt-5">
                  <label for="nama">Nama Pemain</label>
                  <input type="text" class="form-control" id="nama" name="nama">
                </div>
                <div class="form-group">
                  <label for="date">Umur</label>
                  <input type="text" class="form-control" id="umur" name="umur">
                </div>
                <div class="form-group">
                  <label for="bio">Biography</label>
                  <textarea class="form-control" id="bio" rows="3" name="bio"></textarea>
                </div>
                <input type="submit" value="Submit" />
              </form>
        </div>
    </div>
@endsection
