@extends('layouts.master')
@section('title', 'Input Cast Baru')
@section('h1', 'Input Cast Baru')
@section('content')
    <div class="container mb-5">
        <div class="row" style="justify-content: center">
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group mt-5">
                  <label for="nama">Nama Pemain</label>
                  <input type="text" class="form-control" id="nama" name="nama">
                </div>
                <div class="form-group">
                  <label for="date">Umur</label>
                  <input type="text" class="form-control" id="umur" name="umur">
                </div>
                <div class="form-group">
                  <label for="bio">Biography</label>
                  <textarea class="form-control" id="bio" rows="3" name="bio"></textarea>
                </div>
                <input type="submit" value="Submit" />
              </form>
        </div>
    </div>
@endsection
