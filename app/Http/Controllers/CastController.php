<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $casts = DB::table('cast')->get();
        return view('cast', compact('casts'));
    }
    public function create(){
        return view('create');
    }
    public function store(Request $request){
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }
    public function show($id){
        $shows = DB::table('cast')->where('id', $id)->first();
        return view('show', compact('shows'));
    }
    public function destroy($id){
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
    public function edit($id){
        $query = DB::table('cast')->where('id', $id)->first();
        return view('edit', compact(query));
    }
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:post',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }
}
